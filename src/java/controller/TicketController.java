package controller;

import model.Ticket;
import dao.ticketDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "TicketController", urlPatterns = {"/TicketController"})
public class TicketController extends HttpServlet {

    private ticketDAO ticketdao;

    public TicketController() {
        super();
        ticketdao = new ticketDAO();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String forward = "";
        String action = request.getParameter("action");
        if (action.equalsIgnoreCase("delete")) {

            int id = Integer.parseInt(request.getParameter("id"));
            ticketdao.removeTicket(id);

               forward = "listtickets.jsp";
            try {
                request.setAttribute("tickets", ticketdao.getTickets());
            } catch (SQLException e) {
                e.printStackTrace();
            }
            // if the user is trying to edit a person
        } else if (action.equalsIgnoreCase("showtickets")) {
            forward = "listtickets.jsp";
            try {
                request.setAttribute("tickets", ticketdao.getTickets());
            } catch (SQLException e) {
                e.printStackTrace();
            }

        } else {
            forward = "Ticket.jsp";
        }
        RequestDispatcher view = request.getRequestDispatcher(forward);
        view.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Ticket ticket = new Ticket();
        ticket.setDescription(request.getParameter("desc"));

        ticketdao.addTicket(ticket);
        System.out.println("hello");

        response.sendRedirect(request.getContextPath() + "/index.jsp");
    }

}
