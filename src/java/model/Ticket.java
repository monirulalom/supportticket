package model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import dao.TimeAgo;

public class Ticket {

    public Ticket() {
    }

    int id;
    String description;
    Date created_at;
    String status;
    String create_time;
    long interval;
    String ago;

    public int getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getStatus() {
        return status;
    }

    public Date getCreatedAt() {
        return created_at;
    }

    public String getCreate_time() {
        return create_time;
    }

    public long getInterval() {
        return interval;
    }

    public String getAgo() {
        return ago;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDescription(String desc) {
        this.description = desc;
    }

    public void setCreatedAt(Date d) {
        this.created_at = d;
        this.setCreate_time();
        this.setInterval();
        this.setAgo();
        this.setStatus();
    }

    public void setCreate_time() {
        DateFormat dateFormat = new SimpleDateFormat("dd MMM yyyy hh:mm:ss a");
        String t = dateFormat.format(this.created_at);
        this.create_time = t;
    }

    public void setInterval() {
        Date d = new Date();
        long t = d.getTime() - created_at.getTime();
        this.interval = t;
    }

    public void setAgo() {
        this.ago = TimeAgo.toDuration(interval);

    }

    public void setStatus() {
        if(interval <15000)
            this.status = "new";
        else if(interval<30000)
            this.status = "old";
        else
            this.status="expired" ;
    }

}
