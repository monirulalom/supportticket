package dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import model.Ticket;

public class ticketDAO {

    private Connection connection;

    public ticketDAO() {
        dbConnection con = new dbConnection();
        try {
            connection = con.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addTicket(Ticket ticket) {
        try {
            String query = "insert into ticket(description) values ('" + ticket.getDescription() + "')";

            Statement stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeTicket(int id) {
        String query = "delete from ticket where ticket.id = " + id + " ";
        try {
            Statement stmt = connection.createStatement();
            stmt.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Ticket> getTickets() throws SQLException {
        String query = "select * from ticket";
        ArrayList<Ticket> tickets = new ArrayList<Ticket>();
        Statement stmt = connection.createStatement();
        ResultSet res = stmt.executeQuery(query);
        while (res.next()) {
            Ticket ticket = new Ticket();
            ticket.setId(res.getInt("id"));
            ticket.setDescription(res.getString("description"));
            ticket.setCreatedAt(res.getTimestamp("created_at"));
            System.out.println(ticket.getId());
            System.out.println(ticket.getDescription());
            tickets.add(ticket);
        }
        return tickets;
    }
}
