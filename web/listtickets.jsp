<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Support Ticket system</title>
        <link href="bootstrap.css" rel="stylesheet">
        <link href="style.css" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="#">Support</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link"  href="TicketController?action=insert">Submit a new ticket</a>
                    </li>
                </ul>
                <!--form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form-->
            </div>
        </nav>

        <section>
            <h2 class="text-center m-4">All Tickets</h2>
            <div class="container">
                <table class="table table-hover table-dark">
                    <thead>
                        <tr>
                            <th> 
                                Id
                            </th>
                            <th>
                                Description
                            </th>
                            <th>
                                Status
                            </th>
                            <th>
                                Created                                    
                            </th>
                            <th>
                                Exact Time
                            </th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${tickets}" var="ticket">
                            <tr>
                                <td>
                                    <c:out value="${ticket.id}"/>
                                </td>
                                <td>
                                    <c:out value="${ticket.description}"/>
                                </td>
                                <td>
                                    <span class="<c:out value="${ticket.status}"/>"</span>
                                    <c:out value="${ticket.status}"/>
                                </td>

                                <td>
                                    <c:out value="${ticket.ago}"/>
                                </td>
                                <td>
                                    <c:out value="${ticket.create_time}"/>
                                </td>
                                <!--td><a  class="btn btn-primary" href="TicketController?action=edit&id=<c:out value="${ticket.id}"/>">Update</a></td-->
                                <td><a class="btn btn-danger" href="TicketController?action=delete&id=<c:out value="${ticket.id}"/>">Delete</a></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>

            </div>
        </section>	

    </body>
</html>
