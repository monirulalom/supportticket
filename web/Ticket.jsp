<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Support Ticket system</title>
        <link href="bootstrap.css" rel="stylesheet">
        <link href="style.css" rel="stylesheet">
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="/supportticket/">Support</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </nav>
        <h2 class="text-center m-4">Add a new ticket</h2>

        <div class="container">


            <form method="POST" action='TicketController' name="frmAddTicket" role="form" class="text-center">

                <div class="form-group">
                    <label for="desc">Issue:</label>
                    <textarea class="form-control"  id="desc" name="desc" placeholder="Write your issues here."></textarea>
                </div>
                <input type="submit" value="Submit" class="btn btn-primary" />
            </form>
        </div>
    </body>
</html>
